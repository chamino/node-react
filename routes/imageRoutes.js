const axios = require('axios');

module.exports = app => {
    app.get('/api/image', async (req, res) => {
        try {
            const image = await axios.get("https://api.unsplash.com/photos/random/?client_id=ccd4ae8164c664ab3fa429e378aaf48292a44581bd80112e266245ee425ce5ed&orientation=landscape&query=landscape");
            const data = image.data
            res.send(data);
        } catch (error) {
            console.error(error)
            res.send(error);
        } 
    });
};