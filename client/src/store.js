import Vue from "vue";
import Vuex from "vuex";

import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    backgroundImageUrl: "",
    editor: false
  },
  getters: {
    getBackgroundImageUrl(state) {
      return state.backgroundImageUrl;
    },
    getEditor(state) {
      return state.editor;
    }
  },
  mutations: {
    UPDATE_BACKGROUND_IMAGE: (state, url) => {
      state.backgroundImageUrl = url;
    },
    UPDATE_EDITOR: (state, editor) => {
      state.editor = editor;
    }
  },
  actions: {
    getBackgroundImage: async function(context) {
      try {
        let res = await axios.get("/api/image");
        console.log(this.backgroundImage);
        let data = await res.data;
        context.commit("UPDATE_BACKGROUND_IMAGE", data.urls.regular);
      } catch (e) {
        console.error(e);
      }
    },
    setEditor(context, editor) {
      context.commit("UPDATE_EDITOR", editor);
    }
  }
});
