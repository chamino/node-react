FROM node:8.16.0-jessie

WORKDIR /usr/src/server

COPY ./ ./

RUN npm install 

CMD ["/bin/bash"]

