import { combineReducers } from 'redux';
import authReducer from './authReducer';
import {reducer as reduxForm } from 'redux-form';

export default combineReducers({
    auth: authReducer,
    form:  reduxForm
});

console.log(process.env.REACT_APP_STRIPE_KEY);
console.log(process.env.NODE_ENV);